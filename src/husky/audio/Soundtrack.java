package husky.audio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.zip.ZipEntry;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import husky.exception.MissingScriptException;
import husky.utils.Zip;

public class Soundtrack {
	private String currentStage;
	
	/**
	 * Sets the current stage of the music (Used for dynamic music transitions).
	 * @param stage
	 */
	public void setMusicStage(String stage)	{currentStage = stage;}
	
	public Soundtrack(File hesfFile) throws FileNotFoundException, IOException, MissingScriptException, LineUnavailableException, UnsupportedAudioFileException{
		final Zip hesfZip = new Zip(hesfFile);
		
		//Find MusicScript
		ZipEntry[] entries = hesfZip.getAllEntries();
		ZipEntry chosenEntry = null;
		for(int i = 0; i < entries.length; i++){
			if(entries[i].getName().endsWith(".hmus")){		//Found a MusicScript
				chosenEntry = entries[i];
				continue;
			}
		}
		if(chosenEntry == null){	//Could not find MusicScript, throw exception
			throw new MissingScriptException("Failed to find MusicScript in Soundtrack '" + hesfFile.getName() + "'.\nCheck that the script extension is .HMUS.");
		}
		
		//Load audio files
		List<ZipEntry> audioEntries = new ArrayList<ZipEntry>();
		for(int i = 0; i < entries.length; i++){
			if(entries[i].getName().endsWith(".wav")){
				audioEntries.add(entries[i]);
			}
		}
		
		List<Track> audioFiles = new ArrayList<Track>();
		for(int i = 0; i < entries.length; i++){
			audioFiles.add(new Track(AudioSystem.getAudioInputStream(hesfZip.getEntryInputStream(audioEntries.get(i)))));
		}
		
		//Load the entry
		Scanner sc = new Scanner(hesfZip.getEntryInputStream(chosenEntry));
		List<String> data = new ArrayList<String>();
		while(sc.hasNextLine())		data.add(sc.nextLine());
		sc.close();
		
		
	}
	
	public class Script{
		
	}
}
