package husky.audio;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

//Imports
import lib.WaveData;

/**
 * <strong>Only supports WAV 16bit-signed right now.</strong>
 * 
 * @version v2.1
 * @author Myles
 * @since (Old Engine) V1.2
 * @see lib.WaveData
 */
public class Track{
	private WaveData wavData = new WaveData();
	private DataLine.Info info;
	private Clip clip;
	private int[] data;
	
	public void play(){
		clip.start();
	}
	
	public void stop(){
		clip.stop();
	}
	
	public void flush(){
		clip.stop();
		clip.flush();
	}
	
	public void setPosition(int frames){
		clip.setFramePosition(frames);
	}
	
	public int getCurrentAmplitude(){
		return data[clip.getFramePosition()];
	}
	
	public Track(File audioFile) throws LineUnavailableException, IOException, UnsupportedAudioFileException{
		this(AudioSystem.getAudioInputStream(audioFile));
	}
	
	public Track(AudioInputStream audioInputStream) throws LineUnavailableException, IOException{
		info = new DataLine.Info(Clip.class, audioInputStream.getFormat());
		clip = (Clip)AudioSystem.getLine(info);
		clip.open(audioInputStream);
		data = wavData.extractAmplitudeDataFromAudioInputStream(audioInputStream);
	}
}
