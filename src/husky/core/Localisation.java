package husky.core;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Myles
 * @since Build 4
 */
public class Localisation {
	public List<Locale> locales = new ArrayList<Locale>();
	
	protected final void addLocale(Locale locale){
		locales.add(locale);
	}
	
	public final String get(String id){
		//Find entry
		String entry = null;
		for(int i = locales.size() - 1; i >= 0; i--){
			if(entry == null && locales.get(i).stringEntries.containsKey(id)){
				System.out.println(locales.get(i).stringEntries.get(id));
				return locales.get(i).stringEntries.get(id);
			}
		}
		return entry;
	}
	
	/**
	 * Loads all the locales found in the "resources/locales/" folder.
	 */
	public Localisation(){
		//Check if locales directory exists, if not, create it.
		File localeDir = new File("resources/locales/");
		if(!localeDir.isDirectory())	localeDir.mkdirs();
		
		//Get files to load
		File[] localeFiles = localeDir.listFiles();
		List<File> loadList = new ArrayList<File>();
		for(int i = 0; i < localeFiles.length; i++){
			if(localeFiles[i].getName().endsWith(".locale"))	loadList.add(localeFiles[i]);
		}
		
		//Load them
		for(int i = 0; i < loadList.size(); i++){
			try{
				Locale locale = new Locale(loadList.get(i));
				locale.loadPosition = i + 1;
				locales.add(locale);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
}
