package husky.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import husky.exception.SyntaxException;

/**
 * @author Myles
 * @since Build 4
 */
public class Locale{
	//Stuff to make my life easier
	private static final String qm = new String(new char[]{'"'});
	
	//Metadata
	private String localeID = null;
	private String localeName = null;
	public int loadPosition = new Random().nextInt();
	
	//Entries
	protected HashMap<String, String> stringEntries = new HashMap<String, String>();
	protected HashMap<String, File> fileEntries = new HashMap<String, File>();
	
	public Locale(File localeFile){
		
	}
}