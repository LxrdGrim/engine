package husky.exception;

public class MissingScriptException extends Exception{
	private static final long serialVersionUID = -5616291342876704902L;

	public MissingScriptException(){
		super();
	}
	
	public MissingScriptException(String msg){
		super(msg);
	}
}
