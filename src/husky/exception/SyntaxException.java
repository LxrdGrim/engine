package husky.exception;

/**
 * A syntax exception occurs when an error due to syntax occurs in a scripting language that is currently being parsed.
 * 
 * @author Myles
 * @since Husky v0.4
 */
public class SyntaxException extends Exception{
	private static final long serialVersionUID = 3669313682408411808L;
	
	public SyntaxException(){
		super();
	}
	
	public SyntaxException(String msg){
		super(msg);
	}
}
