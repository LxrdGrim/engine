package husky.utils;

import java.io.Closeable;
//File managment
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
//Exceptions
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
//Utils
import java.util.List;
import java.util.ArrayList;
import java.util.Enumeration;
//Zip
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;


/**
 * @author Myles
 * @version v1.1
 * @since EKKO v1.2 (Old 2D Engine)
 */
public class Zip implements Closeable{
	private static final int BUFFER_SIZE = 4;	//In KB
	private static final int BUFFER_SIZE_ACTUAL = BUFFER_SIZE * 1024;		//Converting to bytes
	
	private File file;
	private ZipFile zip;
	private ZipEntry[] entriesArray;
	
	public static void createZip(File output, File[] files) throws IOException{
		FileOutputStream fos = new FileOutputStream(output);
		ZipOutputStream zos = new ZipOutputStream(fos);
		
		for(int i = 0; i < files.length; i++){
			FileInputStream fis = new FileInputStream(files[i]);
			ZipEntry entry = new ZipEntry(files[i].toString());
			zos.putNextEntry(entry);
			
			byte[] buffer = new byte[BUFFER_SIZE_ACTUAL];
			int length;
			while((length = fis.read(buffer)) >= 0){
				zos.write(buffer, 0, length);
			}
			zos.closeEntry();
			fis.close();
		}
		zos.close();
		fos.close();
	}
	
	public Zip(File zipFile) throws FileNotFoundException, IOException{
		if(zipFile.exists()){
			file = zipFile;
			zip = new ZipFile(file);
			Enumeration<? extends ZipEntry> entries = zip.entries();
			zip.close();
			List<ZipEntry> entriesList = new ArrayList<ZipEntry>();
			while(entries.hasMoreElements()){
				entriesList.add(entries.nextElement());
			}
			entriesArray = entriesList.toArray(new ZipEntry[0]);
		}else{
			throw new FileNotFoundException("Could not find specified file: " + zipFile.getAbsolutePath());
		}
	}
	
	public final ZipEntry getEntry(String filename){
		for(int i = 0; i < entriesArray.length; i++){
			if(entriesArray[i].getName() == filename){
				return entriesArray[i];
			}
		}
		return null;
	}
	
	public final ZipEntry[] getFileType(String extension){
		List<ZipEntry> validFileList = new ArrayList<ZipEntry>();
		String ext = checkExtension(extension);
		for(int i = 0; i < entriesArray.length; i++){
			if(entriesArray[i].getName().toLowerCase().endsWith(ext)){
				validFileList.add(entriesArray[i]);
			}
		}
		return validFileList.toArray(new ZipEntry[0]);
	}
	
	private static final String checkExtension(String extension){
		if(extension.startsWith(".")){
			return extension;
		}else{
			return "." + extension;
		}
	}
	
	public InputStream getEntryInputStream(ZipEntry entry) throws IOException{
		return zip.getInputStream(entry);
	}
	
	public File getFile(){
		return file;
	}
	
	public ZipFile getZipFile(){
		return zip;
	}
	
	public ZipEntry[] getAllEntries(){
		return entriesArray;
	}
	
	public void close() throws IOException{
		zip.close();
	}
}
