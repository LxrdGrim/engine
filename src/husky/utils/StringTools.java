package husky.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Basic tools to do with Strings.
 * 
 * @author Myles
 * @since EKKO v1.2 (Old 2D Engine)
 * @version v1.0
 */
public class StringTools {
	/**
	 * @param line - The line to remove all spaces from
	 * @return The line with no spaces
	 */
	public static final String removeAllSpacing(String line){
		//Removing spaces
		char[] lineChars = line.toCharArray();
		List<Character> charsList = new ArrayList<Character>();
		for(int i = 0; i < lineChars.length; i++){
			if(lineChars[i] != ' '){
				charsList.add(lineChars[i]);
			}
		}
		//Reconstructing
		Character[] chars_unformatted = charsList.toArray(new Character[0]);
		char[] chars = new char[chars_unformatted.length];
		for(int i = 0; i < chars_unformatted.length; i++){
			chars[i] = chars_unformatted[i].charValue();
		}
		return new String(chars);
	}
	
	/**
	 * @param str - String to clean up.
	 * @return The integer contained in the String.<br>
	 * This ignores all letters and all punctuation except the first '-'.
	 */
	public static final String cleanInteger(String str){
		final char[] str_char_arr = str.toCharArray();
		List<String> pass1_list = new ArrayList<String>();
		//First pass
		for(int i = 0; i < str_char_arr.length; i++){
			if(str_char_arr[i] == '0' || str_char_arr[i] == '1' || str_char_arr[i] == '2' || str_char_arr[i] == '3' ||
					str_char_arr[i] == '4' || str_char_arr[i] == '5' || str_char_arr[i] == '6' ||
					str_char_arr[i] == '7' || str_char_arr[i] == '8' || str_char_arr[i] == '9' ||
					str_char_arr[i] == '-')
			{
				pass1_list.add(String.valueOf(str_char_arr[i]));
			}
		}
		/*Quick check*/
		if(pass1_list.isEmpty()) return null;
		
		//Reconstructing to String
		final String pass1 = reconstruct(pass1_list.toArray(new String[0]));
		List<String> pass2_list = new ArrayList<String>();
		for(int i = 0; i < pass1.length(); i++){
			if(pass1.charAt(i) == '-' && i == 0){
				pass2_list.add(String.valueOf(pass1.charAt(i)));
			}
			if(pass1.charAt(i) == '0' || pass1.charAt(i) == '1' || pass1.charAt(i) == '2' || pass1.charAt(i) == '3' ||
					pass1.charAt(i) == '4' || pass1.charAt(i) == '5' || pass1.charAt(i) == '6' ||
					pass1.charAt(i) == '7' || pass1.charAt(i) == '8' || pass1.charAt(i) == '9')
			{
				pass2_list.add(String.valueOf(pass1.charAt(i)));
			}
		}
		return reconstruct(pass2_list.toArray(new String[0]));
	}
	
	public static final boolean isTag(String line){
		if(line.contains("<") && line.split("<")[1].contains(">")
				&& !line.split("<")[1].trim().split(">")[0].trim().isEmpty())
		{
			return true;
		}else return false;
	}
	
	public static final String getTagContents(String line) throws Exception{
		if(!isTag(line))	throw new Exception("'" + line + "' does not contain a tag.");
		return line.split("<")[1].trim().split(">")[0].trim().toLowerCase();
	}
	
	public static final boolean isEndTag(String line){
		if(!isTag(line))	return false;
		try{
			if(getTagContents(line).startsWith("/")){
				return true;
			}else{
				return false;
			}
		}catch(Exception e){
			return false;
		}
	}
	
	/**
	 * @param <strong>line</strong> - the line to check
	 * @return <strong>true</strong> if the line is a variable tag,<br>
	 * 		   <strong>false</strong> otherwise.
	 */
	public static final boolean isVariableTag(String line){
		if(!isTag(line))	return false;
		try {
			if(getTagContents(line).contains("="))	return true;
			else return false;
		}catch(Exception e){
			return false;
		}
	}
	
	public static final String getFileExtension(String fileName){
		String[] nameSplit = fileName.split(".");
		return nameSplit[nameSplit.length - 1];
	}
	
	public static final String combine(String[] strArr, String eolCharset){
		String line = null;
		for(int i = 0; i < strArr.length; i++){
			if(i != 0){
				String lineClone = line;
				line = lineClone + strArr[i] + eolCharset;
			}else{
				line = strArr[i] + eolCharset;
			}
		}
		return line;
	}
	
	/**
	 * Reconstructs a String from an array of <code>Chars</code> in <code>String</code> form.<br>
	 * It is private as it has no real use outside of this Utility.
	 * @param charArray
	 */
	private static final String reconstruct(String[] charArray){
		String pass1_line = null;
		for(int i = 0; i < charArray.length; i++){
			if(i != 0){
				String data = pass1_line.toString() + charArray[i];
				pass1_line = data;
			}else{
				pass1_line = charArray[i];
			}
		}
		return pass1_line;
	}
}
